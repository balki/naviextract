package main

import (
	"flag"
	"fmt"

	"github.com/navidrome/navidrome/conf"
	"github.com/navidrome/navidrome/scanner/metadata"
)

func main() {
	flag.Parse()
	conf.Server.Scanner.Extractor = "taglib"
	md, err := metadata.Extract(flag.Args()...)
	for _, v := range md {
		tx, ty := v.TrackNumber()
		dx, dy := v.DiscNumber()
		fmt.Println("Title()              ", v.Title())
		fmt.Println("Album()              ", v.Album())
		fmt.Println("Artist()             ", v.Artist())
		fmt.Println("AlbumArtist()        ", v.AlbumArtist())
		fmt.Println("SortTitle()          ", v.SortTitle())
		fmt.Println("SortAlbum()          ", v.SortAlbum())
		fmt.Println("SortArtist()         ", v.SortArtist())
		fmt.Println("SortAlbumArtist()    ", v.SortAlbumArtist())
		fmt.Println("Composer()           ", v.Composer())
		fmt.Println("Genre()              ", v.Genre())
		fmt.Println("Year()               ", v.Year())
		fmt.Println("DiscSubtitle()       ", v.DiscSubtitle())
		fmt.Println("HasPicture()         ", v.HasPicture())
		fmt.Println("Comment()            ", v.Comment())
		fmt.Println("Lyrics()             ", v.Lyrics())
		fmt.Println("Compilation()        ", v.Compilation())
		fmt.Println("CatalogNum()         ", v.CatalogNum())
		fmt.Println("MbzTrackID()         ", v.MbzTrackID())
		fmt.Println("MbzAlbumID()         ", v.MbzAlbumID())
		fmt.Println("MbzArtistID()        ", v.MbzArtistID())
		fmt.Println("MbzAlbumArtistID()   ", v.MbzAlbumArtistID())
		fmt.Println("MbzAlbumType()       ", v.MbzAlbumType())
		fmt.Println("MbzAlbumComment()    ", v.MbzAlbumComment())
		fmt.Println("Duration()           ", v.Duration())
		fmt.Println("BitRate()            ", v.BitRate())
		fmt.Println("ModificationTime()   ", v.ModificationTime())
		fmt.Println("FilePath()           ", v.FilePath())
		fmt.Println("Suffix()             ", v.Suffix())
		fmt.Println("Size()               ", v.Size())
		fmt.Println("TrackNumber() tx, ty ", tx, ty)
		fmt.Println("DiscNumber() dx, dy  ", dx, dy)
		fmt.Println("---------------------------------------------")
	}
	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
